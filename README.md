# Before use

- Install Python >= 3.6
- Install "flask" either with pip, pipenv or other tools

# Run


- In a windows shell $env:FLASK_APP = "lotto.py"
- Enter "flask run" in a command line into the "lotto" directory
- Load a browser (firefox, chrome, edge, ...) and enter "localhost:5000" into the address bar
