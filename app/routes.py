from flask import render_template
from app import app
import random

@app.route('/')
def homepage():
    # get a new random game
    new_game = list(range(1,91))
    random.shuffle(new_game)
    return render_template('homepage.html', new_game=new_game)